package com.atlassian.plugins.less.monitoring;

import com.atlassian.lesscss.spi.LessCssCompilationEvent;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class LessCompilationStore {
    private static final Map<URI, Long> compilations = new ConcurrentHashMap<>();

    private LessCompilationStore() {
    }

    static long register(LessCssCompilationEvent compilationEvent) {
        URI resourceUri = compilationEvent.getLessResourceUri();
        return compilations.merge(resourceUri, 1L, (k, v) -> v + 1);
    }

    static Map<URI, Long> getCompilations() {
        return new HashMap<>(compilations);
    }
}
